import { Paper,Grid, Typography, List, ListItem, Divider, FormControl, InputLabel, Select, MenuItem, TextField, Button, Dialog } from "@material-ui/core";
import React from "react";
import "./styles.css";
import {yyyymmdd} from ".";
import apiInstance from "../api";



function ProjectView(props){
    let data=props.data;
    return (
        <Grid component={Paper} style={{padding:"10px"}} container>
        <Grid item xs={10}>
        <Typography variant="h5">
        {data.title}
        </Typography>
        <Typography variant="subtitle1">
        {data.detail}
        </Typography>
        </Grid>
        <Grid item xs={2}>
        <Button onClick={(e)=> {e.stopPropagation();window.open(data.file_url, "_blank")}} fullWidth variant="outlined" color="default">OPEN LINK</Button>
        </Grid>
        <Grid item xs={6}>
        <Typography variant="body1">
        Client: {data.client}
        </Typography>
        <Typography variant="body1">
        Role: {data.role}
        </Typography>
        </Grid>
        <Grid item xs={6}>
        <Typography variant="body1">
        {data.type} Project
        </Typography>
        <Typography variant="body1">
        {yyyymmdd(new Date(data.start_date))} TO {yyyymmdd(new Date(data.end_date))}
        </Typography>
        </Grid>
        <Grid item xs={12}>
        </Grid>
        </Grid>
        );
    }
    
    
    class Project extends React.Component{
        constructor(props){
            super(props);
            this.state=({
                projects:[],
                editOpen:false,
                    title:'',
                    type:'',
                    client:'',
                    start_date:yyyymmdd(new Date()),
                    end_date:yyyymmdd(new Date()),
                    role:'',
                    detail:'',
                    file_url:'',
                    id:0,
                operation:'',
                error_msg:null
            });
            this.setState=this.setState.bind(this);
            this.handleEditClose=this.handleEditClose.bind(this);
            this.handleListClick=this.handleListClick.bind(this);
            this.handleAddClick=this.handleAddClick.bind(this);
            this.handleSubmit=this.handleSubmit.bind(this);
            this.handleDelete=this.handleDelete.bind(this);
        }
        componentDidMount(){
            (async ()=>{
                let response=await apiInstance.requestWithAuth("private/project",{id:0});
                if(response && !response.status && !response.error_msg){
                    this.setState({projects:response});
                }
                else this.setState({projects:[]});
            })();
        }
        async handleEditClose(){
            this.setState({editOpen:false});
            let response=await apiInstance.requestWithAuth("private/project",{id:0});
            if(response && !response.status && !response.error_msg){
                this.setState({projects:response});
            }
            else this.setState({projects:[]});
        }
        handleListClick(data){
            data.editOpen=true;
            data.operation="update";
            data.start_date=yyyymmdd(new Date(data.start_date));
            data.end_date=yyyymmdd(new Date(data.end_date));
            data.error_msg=null;
            this.setState(data);
        }
        handleAddClick(){
            this.setState({
                editOpen:true,
                title:'',
                type:'',
                client:'',
                start_date:yyyymmdd(new Date()),
                end_date:yyyymmdd(new Date()),
                role:'',
                detail:'',
                file_url:'',
                id:0,
                operation:"insert",
                error:''
            });
        }
        async handleSubmit(){
            let response=await apiInstance.requestWithAuth("private/dynamic_project",this.state);
            if(response && response.status===1){
                this.handleEditClose();
            }
            else this.setState({error:"All fields are mandatory."});
        }
        async handleDelete(){
            await this.setState({operation:"delete"});
            let response=await apiInstance.requestWithAuth("private/dynamic_project",this.state);
            if(response && response.status===1){
                this.handleEditClose();
            }
            else this.setState({error:response.error});
        }
        render(){
            return (
                <Paper elevation={5}>
                <Grid container>
                <Grid item xs={10}>
                <Typography align="left" variant="h4" style={{padding:"10px"}}>
                Completed Projects
                </Typography>
                
                </Grid>
                <Grid item xs={2}>
                <Button fullWidth variant='outlined' onClick={this.handleAddClick} color='primary'>Add</Button>
                </Grid>
                </Grid>
                <Divider />
                <List alignItems="center" >
                {
                    this.state.projects.length>0?this.state.projects.map((project)=>{
                        return <ListItem button onClick={()=>{this.handleListClick(project)}}><ProjectView data={project} /></ListItem>
                    }):(<ListItem><Typography variant="subtitle1">No Project Records.</Typography></ListItem>)
                }
                </List>
                {/*this.state.editOpen?(<AcademicEdit open={this.state.editOpen} streams={this.state.streams} degrees={this.state.degrees} data={this.state} operation={this.state.operation} onClose={this.handleEditClose} />):(null)*/}
                
                <Dialog open={this.state.editOpen} onClose={this.handleEditClose}>
                <div  style={{height:"100%",padding:"15px"}}>
                <Grid container spacing={1}>
                <Grid item xs={12}>
                <TextField fullWidth label="Title" value={this.state.title} onChange={async (event)=>{await this.setState({title:event.target.value});}} type="text"  name="title" variant="outlined"/>
                </Grid>
                <Grid item sm={6}>
                <FormControl variant="outlined" fullWidth>
                <InputLabel>
                Type
                </InputLabel>
                <Select
                value={this.state.type}
                onChange={async (event)=>{await this.setState({type:event.target.value});}}
                ><MenuItem value="Industrial" key="type">Industrial</MenuItem>
                <MenuItem value="Academic" key="type">Academic</MenuItem>
                <MenuItem value="Personal" key="type">Personal</MenuItem>
                </Select>
                </FormControl>
                </Grid>
                <Grid item xs={6}>
                <TextField fullWidth label="Role" value={this.state.role} onChange={async (event)=>{await this.setState({role:event.target.value});}} type="name"  name="role" variant="outlined"/>
                </Grid>
                <Grid item xs={6}>
                <TextField fullWidth label="Details" multiline value={this.state.detail} onChange={async (event)=>{await this.setState({detail:event.target.value});}} type="text"  name="detail" variant="outlined"/>
                </Grid>
                <Grid item xs={6}>
                <TextField fullWidth label="Client" value={this.state.client} onChange={async (event)=>{await this.setState({client:event.target.value});}} type="name"  name="client" variant="outlined"/>
                </Grid>
                <Grid item xs={4}>
                <TextField fullWidth label="From"  value={this.state.start_date} onChange={async (event)=>{await this.setState({start_date:event.target.value});}} type="date"  name="start_date" variant="outlined"/>
                </Grid>
                <Grid item xs={4}>
                <TextField fullWidth label="To"  value={this.state.end_date} onChange={async (event)=>{await this.setState({end_date:event.target.value});}} type="date"  name="end_date" variant="outlined"/>
                </Grid>
                <Grid item xs={4}>
                <TextField fullWidth label="Associated Link" value={this.state.file_url} onChange={async (event)=>{await this.setState({file_url:event.target.value});}} type="url"  name="file_url" variant="outlined"/>
                </Grid>
                {this.state.operation==='insert'?(
                    <Grid item container spacing={1} xs={12}>
                    <Grid item xs={8}>
                    <Button fullWidth variant="contained" onClick={this.handleSubmit} color="primary">Add</Button>
                    </Grid>
                    <Grid item xs={4}>
                    <Button fullWidth variant="outlined" onClick={this.handleEditClose} color="default">Cancel</Button>
                    </Grid>
                    </Grid>
                    ):(
                        <Grid item container spacing={1} xs={12}>
                        <Grid item xs={4}>
                        <Button fullWidth variant="contained" onClick={this.handleSubmit} color="primary">Update</Button>
                        </Grid>
                        <Grid item xs={4}>
                        <Button fullWidth variant="outlined" onClick={this.handleDelete} color="primary">Delete</Button>
                        </Grid>
                        <Grid item xs={4}>
                        <Button fullWidth variant="outlined" onClick={this.handleEditClose} color="default">Cancel</Button>
                        </Grid>
                        </Grid>
                        )}
                        </Grid>
                        <Typography variant="body2" align="center" color="error">
                            {this.state.error}
                        </Typography>

                        </div>
                        </Dialog>
                        </Paper>
                        );
                    }
                }
                
                export {Project};