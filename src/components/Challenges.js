import { Paper,Grid, Typography, List, ListItem, Divider, TextField, Button, Dialog } from "@material-ui/core";
import React from "react";
import "./styles.css";
import apiInstance from "../api";



function ChallengeView(props){
    const data=props.data, apply=!props.applied, callback=props.callback;
    function action(){
            (async ()=>{
                let response=await apiInstance.requestWithAuth("private/dynamic_challenge_apply",{id:data.challenge_id,operation:apply?"insert":"delete"});
                await callback();
            })();
    }
    return (
        <Grid component={Paper} style={{padding:"10px"}} container>
        <Grid item xs={6}>
        <Typography variant="h5">
        {data.name}
        </Typography>
        <Typography variant="subtitle1">
        {data.desc}
        </Typography>
        </Grid>
        <Grid item xs={4}>
        <Typography variant="subtitle1">
        START: {data.start_date_time}<br/>
        END: {data.end_date_time}
        </Typography>
        </Grid>
        <Grid item xs={2}>
        <Button onClick={(e)=> {e.stopPropagation();action();}} fullWidth variant="outlined" color="default">{apply?"APPLY":"CANCEL"}</Button>
        </Grid>
        <Grid item xs={12}>
        </Grid>
        </Grid>
        );
    }
    
    
    class Challenge extends React.Component{
        category;
        categoryName;
        constructor(props){
            super(props);
            this.category=props.category;
            this.categoryName=props.categoryName;
            this.state=({
                challengesAvaliable:[],
                challengesApplied:[],
            });
            this.setState=this.setState.bind(this);
            this.handleListClick=this.handleListClick.bind(this);
        }
        componentDidMount(){
            this.handleListClick();
        }
        handleListClick(){
            (async ()=>{
                let response=await apiInstance.requestWithAuth("private/challenge_available",{category:this.category});
                if(response && !response.status && !response.error_msg){
                    this.setState({challengesAvailable:response});
                }
                else this.setState({challengesAvailable:[]});
            })();
            (async ()=>{
                let response=await apiInstance.requestWithAuth("private/challenge_applied",{category:this.category});
                if(response && !response.status && !response.error_msg){
                    this.setState({challengesApplied:response});
                }
                else this.setState({challengesApplied:[]});
            })();
        }
        render(){
            return (
                <Grid container spacing={2}>
                <Grid item xs={6}>
            <Paper elevation={5}>
            <Grid container>
            <Grid item xs={10}>
            <Typography align="left" variant="h4" style={{padding:"10px"}}>
            Available {this.categoryName}
            </Typography>
            
            </Grid>
            </Grid>
            <Divider />
            <List alignItems="center" >
            {
                (this.state.challengesAvailable && this.state.challengesAvailable.length)>0?this.state.challengesAvailable.map((challengeAvailable)=>{
                    return <ListItem><ChallengeView callback={this.handleListClick} data={challengeAvailable}/></ListItem>
                }):(<ListItem><Typography variant="subtitle1">No {this.categoryName} Available.</Typography></ListItem>)
            }
            </List>
                    </Paper>
                    </Grid>
                    <Grid item xs={6}>
                <Paper elevation={5}>
                <Grid container>
                <Grid item xs={10}>
                <Typography align="left" variant="h4" style={{padding:"10px"}}>
                Applied {this.categoryName}
                </Typography>
                
                </Grid>
                </Grid>
                <Divider />
                <List alignItems="center" >
                {
                    (this.state.challengesApplied && this.state.challengesApplied.length>0)?this.state.challengesApplied.map((challengeApplied)=>{
                        return <ListItem><ChallengeView callback={this.handleListClick} data={challengeApplied} applied/></ListItem>
                    }):(<ListItem><Typography variant="subtitle1">No {this.categoryName} Applied.</Typography></ListItem>)
                }
                </List>
                        </Paper>
                        </Grid>
                        </Grid>
                        );
                    }
                }
                function Challenges(){
                    return(<Challenge categoryName="Challenges" category={1} />);
                }
                
                export {Challenge,Challenges};