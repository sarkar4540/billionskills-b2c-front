import { Paper,Grid, Typography, List, ListItem, Divider, FormControl, InputLabel, Select, MenuItem, TextField, Button, Dialog, IconButton } from "@material-ui/core";
import {Delete,Add, FullscreenExit} from '@material-ui/icons';
import React from "react";
import "./styles.css";
import apiInstance from "../api";
class Skill extends React.Component{
    constructor(props){
        super(props);
        this.state=({
            error_msg:null,
            skills:[],
            all_skills:[]
        });
        this.setState=this.setState.bind(this);
        this.handleRemove=this.handleRemove.bind(this);
        this.handleAdd=this.handleAdd.bind(this);
        this.handleSubmit=this.handleSubmit.bind(this);
    }
    componentDidMount(){
        (async ()=>{
            let response=await apiInstance.requestWithAuth("private/user_skill",{});
            if(!response.status && !response.error_msg){
                this.setState({skills:response});
            }
            else this.setState({skills:[]});
            (async ()=>{
                let response2=await apiInstance.request("public/skills",{});
                if(!response2.status && !response2.error_msg){
                    /*
                    let all_skills=[];
                    response2.forEach(()=>{
                        if(response.indexOf({response2.}))
                    })
                    this.setState({all_skills:all_skills});
                    */
                    this.setState({all_skills:response2});
                    
                }
                else this.setState({all_skills:[]});
            })();
        })();
    }
    handleRemove(data){
        data.error_msg=null;
        this.setState(data);
    }
    handleAdd(data){
        let skills=this.state.skills;
        skills.push(data);
        this.setState({
            skills:skills
        });
        this.handleSubmit();
    }
    async handleSubmit(){
        let data={skill_id:[]};
        await this.state.skills.forEach((skill)=>{data.skill_id.push(skill.id)});
        await apiInstance.requestWithAuth("private/update_skill",data);
        let response=await apiInstance.requestWithAuth("private/user_skill",{});
        if(!response.status && !response.error_msg){
            this.setState({skills:response});
        }
        else this.setState({skills:[]});
    }
    async handleDelete(data){
        let skills=this.state.skills;
        const index = skills.indexOf(data);
        if (index > -1) {
            await skills.splice(index, 1);
        }
        this.setState({
            skills:skills
        });
        await this.handleSubmit();
    }
    render(){
        return (
            <Paper elevation={5} style={{display:"flex",verticalAlign:"center",alignItems:"middle"}}>
            <Typography paragraph align="left" variant="h4" style={{display:"inline-block",padding:"10px"}}>
            Your Skills
            </Typography>
            <div style={{padding:"5px",alignItems:"left",display:"inline-block"}}>
            {
                this.state.skills.length>0?this.state.skills.map((skill)=>{
                    return <Paper style={{padding:"5px",margin:"5px",display:"inline-block"}} >
                    
                    <Typography display="inline" variant="h5">
                    {skill.name}
                    </Typography><IconButton onClick={()=>{this.handleDelete(skill)}} aria-label="delete">
                    <Delete/>
                    </IconButton></Paper>
                }):(null)
            }
            </div>
            <div style={{display:"inline-block"}}>
            <Paper style={{width:"125px",padding:"5px",margin:"5px"}} >
                <FormControl variant="outlined" fullWidth>
                <InputLabel>
                    Add Skill
                </InputLabel>
            <Select
            onChange={async (event)=>{await this.handleAdd(event.target.value);}}
            > 
            {
                this.state.all_skills.length>0?this.state.all_skills.map((skill)=>{
                    return <MenuItem value={skill} key="skill_id">{skill.name}</MenuItem>
                }):(null)
            }
            </Select>
            </FormControl>
            </Paper>
            </div>
            
            </Paper>
            );
        }
    }
    
    export {Skill};