import { Paper,Grid, Typography, List, ListItem, Divider, FormControl, InputLabel, Select, MenuItem, TextField, Button, Dialog, IconButton } from "@material-ui/core";
import {Delete,Add} from '@material-ui/icons';
import React from "react";
import "./styles.css";
import apiInstance from "../api";

class Hobby extends React.Component{
    constructor(props){
        super(props);
        this.state=({
            error_msg:null,
            hobbys:[],
            all_hobbys:[]
        });
        this.setState=this.setState.bind(this);
        this.handleRemove=this.handleRemove.bind(this);
        this.handleAdd=this.handleAdd.bind(this);
        this.handleSubmit=this.handleSubmit.bind(this);
    }
    componentDidMount(){
        (async ()=>{
            let response=await apiInstance.requestWithAuth("private/user_hobby",{});
            if(!response.status && !response.error_msg){
                this.setState({hobbys:response});
            }
            else this.setState({hobbys:[]});
            (async ()=>{
                let response2=await apiInstance.request("public/hobby",{});
                if(!response2.status && !response2.error_msg){
                    /*
                    let all_hobbys=[];
                    response2.forEach(()=>{
                        if(response.indexOf({response2.}))
                    })
                    this.setState({all_hobbys:all_hobbys});
                    */
                    this.setState({all_hobbys:response2});
                    
                }
                else this.setState({all_hobbys:[]});
            })();
        })();
    }
    handleRemove(data){
        data.error_msg=null;
        this.setState(data);
    }
    handleAdd(data){
        let hobbys=this.state.hobbys;
        hobbys.push(data);
        this.setState({
            hobbys:hobbys
        });
        this.handleSubmit();
    }
    async handleSubmit(){
        let data={hobby_id:[]};
        await this.state.hobbys.forEach((hobby)=>{data.hobby_id.push(hobby.id)});
        await apiInstance.requestWithAuth("private/update_hobby",data);
        let response=await apiInstance.requestWithAuth("private/user_hobby",{});
        if(!response.status && !response.error_msg){
            this.setState({hobbys:response});
        }
        else this.setState({hobbys:[]});
    }
    async handleDelete(data){
        let hobbys=this.state.hobbys;
        const index = hobbys.indexOf(data);
        if (index > -1) {
            await hobbys.splice(index, 1);
        }
        this.setState({
            hobbys:hobbys
        });
        await this.handleSubmit();
    }
    render(){
        return (
            <Paper elevation={5}>
            <Typography align="left" variant="h4" style={{padding:"10px"}}>
            Hobbies
            </Typography>
            <Divider />
            <Grid container style={{padding:"5px"}} alignItems="center" spacing={1}>
            {
                this.state.hobbys.length>0?this.state.hobbys.map((hobby)=>{
                    return <Grid item xs={3}><Paper style={{padding:"5px"}} >
                    
                    <Typography display="inline" variant="h5">
                    {hobby.title}
                    </Typography><IconButton onClick={()=>{this.handleDelete(hobby)}} aria-label="delete">
                    <Delete/>
                    </IconButton></Paper></Grid>
                }):(null)
            }
            <Grid item xs={3}><Paper style={{padding:"5px"}} >
                <FormControl variant="outlined" fullWidth>
                <InputLabel>
                    Add Hobby
                </InputLabel>
            <Select fullWidth
            onChange={async (event)=>{await this.handleAdd(event.target.value);}}
            > 
            {
                this.state.all_hobbys.length>0?this.state.all_hobbys.map((hobby)=>{
                    return <MenuItem value={hobby} key="hobby_id">{hobby.title}</MenuItem>
                }):(null)
            }
            </Select>
            </FormControl>
            </Paper></Grid>
            </Grid>
            
            </Paper>
            );
        }
    }
    
    export {Hobby};