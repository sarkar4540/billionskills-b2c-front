import { Paper,Grid, Typography, List, ListItem, Divider, FormControl, InputLabel, Select, MenuItem, TextField, Button, Dialog } from "@material-ui/core";
import React from "react";
import "./styles.css";
import {yyyymmdd} from "./";
import apiInstance from "../api";



function ProfessionalView(props){
    let data=props.data;
    return (
        <Grid component={Paper} style={{padding:"10px"}} container>
        <Grid item xs={10}>
        <Typography variant="h5">
        {data.company}
        </Typography>
        <Typography variant="h6">
        {data.designation}
        </Typography>
        </Grid>
        <Grid item xs={2}>
        <Button onClick={(e)=> {e.stopPropagation();window.open(data.website, "_blank")}} fullWidth variant="outlined" color="default">OPEN WEBSITE</Button>
        </Grid>
        <Grid item xs={6}>
        <Typography variant="body1">
        {data.industry}
        </Typography>
        <Typography variant="body1">
        CTC: {data.current_ctc}
        </Typography>
        </Grid>
        <Grid item xs={6}>
        <Typography variant="body1">
        {yyyymmdd(new Date(data.from))} TO {yyyymmdd(new Date(data.to))}
        </Typography>
        <Typography variant="body1">
        Notice Period: {data.notice_period} days
        </Typography>
        </Grid>
        <Grid item xs={12}>
        </Grid>
        </Grid>
        );
    }
    
    
    class Professional extends React.Component{
        constructor(props){
            super(props);
            this.state=({
                professionals:[],
                editOpen:false,
                    designation_id:0,
                    industry_id:0,
                    company:'',
                    website:'',
                    from:'',
                    to:'',
                    current_ctc:0,
                    notice_period:0,
                    id:0,
                operation:'',
                industrys:[],
                error_msg:null,
                designations:[]
            });
            this.setState=this.setState.bind(this);
            this.handleEditClose=this.handleEditClose.bind(this);
            this.handleListClick=this.handleListClick.bind(this);
            this.handleAddClick=this.handleAddClick.bind(this);
            this.handleSubmit=this.handleSubmit.bind(this);
            this.handleDelete=this.handleDelete.bind(this);
        }
        componentDidMount(){
            (async ()=>{
                let response=await apiInstance.requestWithAuth("private/professional",{id:0});
                if(response && !response.status && !response.error_msg){
                    this.setState({professionals:response});
                }
                else this.setState({professionals:[]});
            })();
            (async ()=>{
                let response=await apiInstance.request("public/industry",{});
                if(response && !response.status && !response.error_msg){
                    this.setState({industrys:response});
                }
                else this.setState({industrys:[]});
            })();
            (async ()=>{
                let response=await apiInstance.request("public/designation",{});
                if(response && !response.status && !response.error_msg){
                    this.setState({designations:response});
                }
                else this.setState({designations:[]});
            })();
        }
        async handleEditClose(){
            this.setState({editOpen:false});
            let response=await apiInstance.requestWithAuth("private/professional",{id:0});
            if(response && !response.status && !response.error_msg){
                this.setState({professionals:response});
            }
            else this.setState({professionals:[]});
        }
        handleListClick(data){
            data.editOpen=true;
            data.operation="update";
            data.to=yyyymmdd(new Date(data.to));
            data.from=yyyymmdd(new Date(data.from));
            data.error_msg=null;
            this.setState(data);
        }
        handleAddClick(){
            this.setState({
                editOpen:true,
                    designation_id:0,
                    industry_id:0,
                    company:'',
                    website:'',
                    current_ctc:0,
                    notice_period:0,
                    id:0,
                from:yyyymmdd(new Date()),
                to:yyyymmdd(new Date()),
                operation:"insert",
                error:''
            });
        }
        async handleSubmit(){
            let response=await apiInstance.requestWithAuth("private/dynamic_professional",this.state);
            if(response.status===1){
                this.handleEditClose();
            }
            else this.setState({error:"All fields are mandatory."});
        }
        async handleDelete(){
            await this.setState({operation:"delete"});
            let response=await apiInstance.requestWithAuth("private/dynamic_professional",this.state);
            if(response.status===1){
                this.handleEditClose();
            }
            else this.setState({error:response.error});
        }
        render(){
            return (
                <Paper elevation={5}>
                <Grid container>
                <Grid item xs={10}>
                <Typography align="left" variant="h4" style={{padding:"10px"}}>
                Professionals
                </Typography>
                
                </Grid>
                <Grid item xs={2}>
                <Button fullWidth variant='outlined' onClick={this.handleAddClick} color='primary'>Add</Button>
                </Grid>
                </Grid>
                <Divider />
                <List alignItems="center" >
                {
                    this.state.professionals.length>0?this.state.professionals.map((professional)=>{
                        return <ListItem button onClick={()=>{this.handleListClick(professional)}}><ProfessionalView data={professional} /></ListItem>
                    }):(<ListItem><Typography variant="subtitle1">No Professional Records.</Typography></ListItem>)
                }
                </List>
                {/*this.state.editOpen?(<AcademicEdit open={this.state.editOpen} streams={this.state.streams} degrees={this.state.degrees} data={this.state} operation={this.state.operation} onClose={this.handleEditClose} />):(null)*/}
                
                <Dialog open={this.state.editOpen} onClose={this.handleEditClose}>
                <div  style={{height:"100%",padding:"15px"}}>
                <Grid container spacing={1}>
                <Grid item sm={6}>
                <FormControl variant="outlined" fullWidth>
                <InputLabel>
                Designation
                </InputLabel>
                <Select
                value={this.state.designation_id}
                onChange={async (event)=>{await this.setState({designation_id:event.target.value});}}
                >
                {this.state.designations.map(designation=>{ return <MenuItem value={designation.id} key="designation">{designation.name}</MenuItem>})}
                </Select>
                </FormControl>
                </Grid>
                <Grid item sm={6}>
                <FormControl variant="outlined" fullWidth>
                <InputLabel>
                Industry
                </InputLabel>
                <Select
                value={this.state.industry_id}
                onChange={async (event)=>{await this.setState({industry_id:event.target.value});}}
                >
                {this.state.industrys.map(industry=>{ return <MenuItem value={industry.id} key="industry">{industry.name}</MenuItem>})}
                </Select>
                </FormControl>
                </Grid>
                <Grid item xs={6}>
                <TextField fullWidth label="Company" value={this.state.company} onChange={async (event)=>{await this.setState({company:event.target.value});}} type="name"  name="company" variant="outlined"/>
                </Grid>
                <Grid item xs={6}>
                <TextField fullWidth label="website" value={this.state.website} onChange={async (event)=>{await this.setState({website:event.target.value});}} type="url"  name="website" variant="outlined"/>
                </Grid>
                <Grid item xs={4}>
                <TextField fullWidth label="From"  value={this.state.from} onChange={async (event)=>{await this.setState({from:event.target.value});}} type="date"  name="from" variant="outlined"/>
                </Grid>
                <Grid item xs={4}>
                <TextField fullWidth label="To"  value={this.state.to} onChange={async (event)=>{await this.setState({to:event.target.value});}} type="date"  name="to" variant="outlined"/>
                </Grid>
                <Grid item xs={4}>
                <TextField fullWidth label="CTC" value={this.state.current_ctc} onChange={async (event)=>{await this.setState({current_ctc:event.target.value});}} type="number"  name="current_ctc" variant="outlined"/>
                </Grid>
                <Grid item xs={4}>
                <TextField fullWidth label="Notice Period (in days)" value={this.state.notice_period} onChange={async (event)=>{await this.setState({notice_period:event.target.value});}} type="number"  name="notice_period" variant="outlined"/>
                </Grid>
                {this.state.operation==='insert'?(
                    <Grid item container spacing={1} xs={12}>
                    <Grid item xs={8}>
                    <Button fullWidth variant="contained" onClick={this.handleSubmit} color="primary">Add</Button>
                    </Grid>
                    <Grid item xs={4}>
                    <Button fullWidth variant="outlined" onClick={this.handleEditClose} color="default">Cancel</Button>
                    </Grid>
                    </Grid>
                    ):(
                        <Grid item container spacing={1} xs={12}>
                        <Grid item xs={4}>
                        <Button fullWidth variant="contained" onClick={this.handleSubmit} color="primary">Update</Button>
                        </Grid>
                        <Grid item xs={4}>
                        <Button fullWidth variant="outlined" onClick={this.handleDelete} color="primary">Delete</Button>
                        </Grid>
                        <Grid item xs={4}>
                        <Button fullWidth variant="outlined" onClick={this.handleEditClose} color="default">Cancel</Button>
                        </Grid>
                        </Grid>
                        )}
                        </Grid>
                        <Typography variant="body2" align="center" color="error">
                            {this.state.error}
                        </Typography>

                        </div>
                        </Dialog>
                        </Paper>
                        );
                    }
                }
                
                export {Professional};