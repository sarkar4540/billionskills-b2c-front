import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
        main: '#d9113d'
    },
    secondary: {
        main: '#a31738'
    },
    success: {
        main: '#29d911'
    },
    error: {
        main: '#d9c111'
    },
    info: {
        main: '#e68ea0'
    },
    warning: {
        main: '#d9cc68'
    }
  },
});

export {theme};