import { Paper,Grid, Typography, List, ListItem, Divider, FormControl, InputLabel, Select, MenuItem, TextField, Button, Dialog } from "@material-ui/core";
import React from "react";
import "./styles.css";
import {yyyymmdd} from "./";
import apiInstance from "../api";



function AcademicView(props){
    let data=props.data;
    return (
        <Grid component={Paper} style={{padding:"10px"}} container>
        <Grid item xs={12}>
        <Typography variant="h5">
        {data.degree}
        </Typography>
        </Grid>
        <Grid item xs={6}>
        <Typography variant="h6">
        {data.stream}
        </Typography>
        <Typography variant="body1">
        {data.organization} ( {data.board} )
        </Typography>
        </Grid>
        <Grid item xs={6}>
        <Typography variant="body1">
        {yyyymmdd(new Date(data.from))} TO {yyyymmdd(new Date(data.to))}
        </Typography>
        <Typography variant="body1">
        {data.marks_per} % obtained
        </Typography>
        </Grid>
        <Grid item xs={12}>
        </Grid>
        </Grid>
        );
    }
    
    
    class Academic extends React.Component{
        constructor(props){
            super(props);
            this.state=({
                academics:[],
                editOpen:false,
                    degree_id:0,
                    stream_id:0,
                    board:'',
                    organization:'',
                    from:'',
                    to:'',
                    marks_per:0,
                    id:0,
                operation:'',
                streams:[],
                error_msg:null,
                degrees:[]
            });
            this.setState=this.setState.bind(this);
            this.handleEditClose=this.handleEditClose.bind(this);
            this.handleListClick=this.handleListClick.bind(this);
            this.handleAddClick=this.handleAddClick.bind(this);
            this.handleSubmit=this.handleSubmit.bind(this);
            this.handleDelete=this.handleDelete.bind(this);
        }
        componentDidMount(){
            (async ()=>{
                let response=await apiInstance.requestWithAuth("private/academic",{id:0});
                if(response && !response.status && !response.error_msg){
                    this.setState({academics:response});
                }
                else this.setState({academics:[]});
            })();
            (async ()=>{
                let response=await apiInstance.request("public/stream",{});
                if(response && !response.status && !response.error_msg){
                    this.setState({streams:response});
                }
                else this.setState({streams:[]});
            })();
            (async ()=>{
                let response=await apiInstance.request("public/degree",{});
                if(response && !response.status && !response.error_msg){
                    this.setState({degrees:response});
                }
                else this.setState({degrees:[]});
            })();
        }
        async handleEditClose(){
            this.setState({editOpen:false});
            let response=await apiInstance.requestWithAuth("private/academic",{id:0});
            if(response && !response.status && !response.error_msg){
                this.setState({academics:response});
            }
            else this.setState({academics:[]});
        }
        handleListClick(data){
            data.editOpen=true;
            data.operation="update";
            data.to=yyyymmdd(new Date(data.to));
            data.from=yyyymmdd(new Date(data.from));
            data.error_msg=null;
            this.setState(data);
        }
        handleAddClick(){
            this.setState({
                editOpen:true,
                degree_id:null,
                stream_id:null,
                board:null,
                organization:null,
                from:yyyymmdd(new Date()),
                to:yyyymmdd(new Date()),
                marks_per:null,
                id:0,
                operation:"insert",
                error:''
            });
        }
        async handleSubmit(){
            let response=await apiInstance.requestWithAuth("private/dynamic_academic",this.state);
            if(response.status===1){
                this.handleEditClose();
            }
            else this.setState({error:"All fields are mandatory."});
        }
        async handleDelete(){
            await this.setState({operation:"delete"});
            let response=await apiInstance.requestWithAuth("private/dynamic_academic",this.state);
            if(response.status===1){
                this.handleEditClose();
            }
            else this.setState({error:response.error});
        }
        render(){
            return (
                <Paper elevation={5}>
                <Grid container>
                <Grid item xs={10}>
                <Typography align="left" variant="h4" style={{padding:"10px"}}>
                Academics
                </Typography>
                
                </Grid>
                <Grid item xs={2}>
                <Button fullWidth variant='outlined' onClick={this.handleAddClick} color='primary'>Add</Button>
                </Grid>
                </Grid>
                <Divider />
                <List alignItems="center" >
                {
                    this.state.academics.length>0?this.state.academics.map((academic)=>{
                        return <ListItem button onClick={()=>{this.handleListClick(academic)}}><AcademicView data={academic} /></ListItem>
                    }):(<ListItem><Typography variant="subtitle1">No Academic Records.</Typography></ListItem>)
                }
                </List>
                {/*this.state.editOpen?(<AcademicEdit open={this.state.editOpen} streams={this.state.streams} degrees={this.state.degrees} data={this.state} operation={this.state.operation} onClose={this.handleEditClose} />):(null)*/}
                
                <Dialog open={this.state.editOpen} onClose={this.handleEditClose}>
                <div  style={{height:"100%",padding:"15px"}}>
                <Grid container spacing={1}>
                <Grid item sm={6}>
                <FormControl variant="outlined" fullWidth>
                <InputLabel>
                Degree
                </InputLabel>
                <Select
                value={this.state.degree_id}
                onChange={async (event)=>{await this.setState({degree_id:event.target.value});}}
                >
                {this.state.degrees.map(degree=>{ return <MenuItem value={degree.id} key="degree">{degree.degree_name}</MenuItem>})}
                </Select>
                </FormControl>
                </Grid>
                <Grid item sm={6}>
                <FormControl variant="outlined" fullWidth>
                <InputLabel>
                Stream
                </InputLabel>
                <Select
                value={this.state.stream_id}
                onChange={async (event)=>{await this.setState({stream_id:event.target.value});}}
                >
                {this.state.streams.map(stream=>{ return <MenuItem value={stream.id} key="stream">{stream.title}</MenuItem>})}
                </Select>
                </FormControl>
                </Grid>
                <Grid item xs={6}>
                <TextField fullWidth label="Organization" value={this.state.organization} onChange={async (event)=>{await this.setState({organization:event.target.value});}} type="name"  name="organization" variant="outlined"/>
                </Grid>
                <Grid item xs={6}>
                <TextField fullWidth label="Board" value={this.state.board} onChange={async (event)=>{await this.setState({board:event.target.value});}} type="name"  name="organization" variant="outlined"/>
                </Grid>
                <Grid item xs={4}>
                <TextField fullWidth label="From"  value={this.state.from} onChange={async (event)=>{await this.setState({from:event.target.value});}} type="date"  name="from" variant="outlined"/>
                </Grid>
                <Grid item xs={4}>
                <TextField fullWidth label="To"  value={this.state.to} onChange={async (event)=>{await this.setState({to:event.target.value});}} type="date"  name="to" variant="outlined"/>
                </Grid>
                <Grid item xs={4}>
                <TextField fullWidth label="Percentage Obtained" value={this.state.marks_per} onChange={async (event)=>{await this.setState({marks_per:event.target.value});}} type="number"  name="marks_per" variant="outlined"/>
                </Grid>
                {this.state.operation==='insert'?(
                    <Grid item container spacing={1} xs={12}>
                    <Grid item xs={8}>
                    <Button fullWidth variant="contained" onClick={this.handleSubmit} color="primary">Add</Button>
                    </Grid>
                    <Grid item xs={4}>
                    <Button fullWidth variant="outlined" onClick={this.handleEditClose} color="default">Cancel</Button>
                    </Grid>
                    </Grid>
                    ):(
                        <Grid item container spacing={1} xs={12}>
                        <Grid item xs={4}>
                        <Button fullWidth variant="contained" onClick={this.handleSubmit} color="primary">Update</Button>
                        </Grid>
                        <Grid item xs={4}>
                        <Button fullWidth variant="outlined" onClick={this.handleDelete} color="primary">Delete</Button>
                        </Grid>
                        <Grid item xs={4}>
                        <Button fullWidth variant="outlined" onClick={this.handleEditClose} color="default">Cancel</Button>
                        </Grid>
                        </Grid>
                        )}
                        </Grid>
                        <Typography variant="body2" align="center" color="error">
                            {this.state.error}
                        </Typography>

                        </div>
                        </Dialog>
                        </Paper>
                        );
                    }
                }
                
                export {Academic};