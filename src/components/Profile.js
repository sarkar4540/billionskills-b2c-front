import { Paper, TextField, Grid,Snackbar, Button, Typography, Select, InputLabel, FormControl, MenuItem, Divider } from "@material-ui/core";
import MuiAlert from '@material-ui/lab/Alert';
import {Academic,Professional} from "./";
import React,{useState} from "react";
import {withRouter} from "react-router-dom";
import "./styles.css";
import apiInstance from "../api";

function yyyymmdd(date) {
    var mm = date.getMonth() + 1; // getMonth() is zero-based
    var dd = date.getDate();
  
    return [date.getFullYear(),
            (mm>9 ? '' : '0') + mm,
            (dd>9 ? '' : '0') + dd
           ].join('-');
    };

class CitySelect extends React.Component{
    constructor(props){
        super(props);
        this.state={
            city: props.city,
            country:0,
            state:0,
            cities:[],
            countries:[],
            states:[]
        }
    }
    async fetchCityDetails(){
        let response=await apiInstance.request('public/city_details',{city_id:this.state.city});
        if(response && !response.status)this.setState({city:response.city_id,state:response.state_id,country:response.country_id});    
    }
    async fetchCities(){
        let response=await apiInstance.request('public/city',{state_id:this.state.state});
        if(response && !response.status)this.setState({cities:response});
        else this.setState({cities:[]});
        this.forceUpdate();
    }
    async fetchStates(){
        let response=await apiInstance.request('public/state',{country_id:this.state.country});
        if(response && !response.status)this.setState({states:response});
        else this.setState({states:[]});
        this.forceUpdate();
    }
    async fetchCountries(){
        let response=await apiInstance.request('public/country',{});
        if(response && !response.status){
            this.setState({countries:response});
        }
        else this.setState({countries:[]});
    }
    async updateCity(){
        apiInstance.requestWithAuth('private/update_user_detail',{
            field_name:'city_id',
            field_data:this.state.city
        });
    }
    componentDidMount(){
        (async ()=>{
            if(this.state.city){
                await this.fetchCityDetails();
                await this.fetchCountries();
                await this.fetchStates();
                await this.fetchCities();
            }
            else{
                await this.fetchCountries();
                await this.setState({country:this.state.countries[0].id});
                await this.fetchStates();
                await this.setState({state:this.state.states[0].id});
                await this.fetchCities();
            }
        })();
    }
    render(){
        return(
            <Grid container spacing={1}>
            <Grid item sm={4}>
            <FormControl variant="outlined" fullWidth>
        <InputLabel>
          Country
        </InputLabel>
        <Select
          value={this.state.country}
          onChange={async (event)=>{await this.setState({country:event.target.value});await this.fetchStates();}}
        >
          {this.state.countries.map(country=>{ return <MenuItem value={country.id}>{country.name}</MenuItem>})}
        </Select>
      </FormControl>
            </Grid>
            <Grid item sm={4}>
            <FormControl variant="outlined" fullWidth>
        <InputLabel>
          State
        </InputLabel>
        <Select
          value={this.state.state}
          onChange={async (event)=>{await this.setState({state:event.target.value});await this.fetchCities();}}
        >
          {this.state.states.map(state=>{ return <MenuItem value={state.id}>{state.name}</MenuItem>})}
        </Select>
      </FormControl>
            </Grid>
            <Grid item sm={4}>
            <FormControl variant="outlined" fullWidth>
        <InputLabel>
          City
        </InputLabel>
        <Select
          value={this.state.city}
          onChange={async (event)=>{await this.setState({city:event.target.value});await this.updateCity();}}
        >
          {this.state.cities.map(city=>{ return <MenuItem value={city.id}>{city.name}</MenuItem>})}
        </Select>
      </FormControl>
            </Grid>
            </Grid>
        )
    }
}

class MothertongueSelect extends React.Component{
    constructor(props){
        super(props);
        this.state={
            mothertongue: props.mothertongue,
            mothertongues:[]
        }
    }
    async fetchMothertongues(){
        let response=await apiInstance.request('public/mothertongue',{});
        if(response && !response.status && !response.error_msg){
            this.setState({mothertongues:response});
        }
        else this.setState({mothertongues:[]});
    }
    async updateMothertongue(){
        apiInstance.requestWithAuth('private/update_user_detail',{
            field_name:'mothertongue_id',
            field_data:this.state.mothertongue
        });
    }
    componentDidMount(){
        (async ()=>{
            await this.fetchMothertongues();
            if(!this.state.mothertongue){
                await this.fetchMothertongues();
                await this.setState({mothertongue:this.state.mothertongues[0].id});
            }
        })();
    }
    render(){
        return(
            <FormControl variant="outlined" fullWidth>
        <InputLabel>
          Mothertongue
        </InputLabel>
        <Select
          value={this.state.mothertongue}
          onChange={async (event)=>{await this.setState({mothertongue:event.target.value});await this.updateMothertongue();}}
        >
          {this.state.mothertongues.map(mothertongue=>{ return <MenuItem value={mothertongue.id}>{mothertongue.lang}</MenuItem>})}
        </Select>
      </FormControl>
        )
    }
}


let ProfileComponent=withRouter((props)=>{
    const userDetails=JSON.parse(props.data);
    console.log(props.data);
    const [error,setError]=useState('');
    const [showError,showErrorSet]=useState(false);
    const [firstName,setFirstName]=useState(userDetails.first_name);
    const [lastName,setLastName]=useState(userDetails.last_name);
    const [fathersName,setFathersName]=useState(userDetails.fathers_name);
    const [fathersOccupation,setFathersOccupation]=useState(userDetails.fathers_occupation);
    const [mothersName,setMothersName]=useState(userDetails.mothers_name);
    const [mothersOccupation,setMothersOccupation]=useState(userDetails.mothers_occupation);
    const [email,setEmail]=useState(userDetails.email);
    const [phone,setPhone]=useState(userDetails.phone);
    const [altEmail,setAltEmail]=useState(userDetails.alt_email);
    const [altPhone,setAltPhone]=useState(userDetails.alt_phone);
    const [pass,setPass]=useState('');
    const [gender,setGender]=useState(userDetails.gender);
    const [newPass,setNewPass]=useState('');
    const [address,setAddress]=useState(userDetails.address);
    const [maritalstatus,setMaritalstatus]=useState(userDetails.maritalstatus);
    const [bloodgroup,setBloodgroup]=useState(userDetails.bloodgroup);
    const [pin,setPin]=useState(userDetails.pin);
    const [caste,setCaste]=useState(userDetails.caste);
    const [physicalChallenge,setPhysicalChallenge]=useState(userDetails.physical_challenge);
    const [percentagePh,setPercentagePh]=useState(userDetails.percentage_ph);
    const [adhaarNo,setAdhaarNo]=useState(userDetails.aadhar_no);
    const [passportNo,setPassportNo]=useState(userDetails.passport_no);
    const [website,setWebsite]=useState(userDetails.website);
    const [facebookLink,setFacebookLink]=useState(userDetails.facebook_link);
    const [linkedinLink,setLinkedInLink]=useState(userDetails.linkedin_link);
    const [googleLink,setGoogleLink]=useState(userDetails.google_link);
    const [aboutMe,setAboutMe]=useState(userDetails.about_me);
    const [expectedCTC,setExpectedCTC]=useState(userDetails.expected_ctc);
    const [resumeHeading,setResumeHeading]=useState(userDetails.resume_heading);
    const [dateOfBirth,setDateOfBirth]=useState(userDetails.dateofbirth);
    const [passwordPanelHidden,setPasswordPanelHidden]=useState(true);
    const [passwordButtonHidden,setPasswordButtonHidden]=useState(false);
    
    function handleChangeProfilePicture(){
        //TODO: implement
    }
    function handleChangePassword(){
        setPasswordButtonHidden(true);
        setPasswordPanelHidden(false);
    }
    function handleCloseChangePassword(){
        setPasswordButtonHidden(false);
        setPasswordPanelHidden(true);
    }
    function handleSaveChangePassword(){
        apiInstance.requestWithAuth('private/change_password',{
            new_password:newPass,
            password:pass
        }).then((value)=>{
            if(value.status===1){
                setError('Password was successfully updated. You will be logged out in 3 seconds. Please Log in again.');
                apiInstance.logOut();
                setTimeout(()=>{
                    props.history.push('/');
                },3000);
            }
            else if(value.status===4){
                setError(value.error);
            }
            else{
                setError('Password was not updated.');
            }
            showErrorSet(true);
        })
    }
    function updateDetailsField(event){
        console.dir(event);
        apiInstance.requestWithAuth('private/update_user_detail',{
            field_name:event.target.name,
            field_data:event.target.value
        });
    }
    const section = {
        height: "100%"
      };
    return (
        <Grid container spacing={1}>
        <Grid item sm={4}>
        <Paper style={section} elevation={5}>
        <form >
        <Grid container justify="center" spacing={2}>
        <Grid item container spacing={2} xs={10}>
        <Grid item xs={12} style={{height:'25px'}}></Grid>
        <Grid item xs={4} style={{textAlign:"center"}}>
        <img style={{width:"100%"}} src='logo.png' alt="BillionSkills"/>
        </Grid>
        <Grid container item xs={8}>
        <Grid item xs={12}>
        <Typography variant="h4">{firstName} {lastName}</Typography>
        <Typography variant="h5">{email}</Typography>
        </Grid>
        <Grid item xs={12}>
        <Button fullWidth variant="outlined" onClick={handleChangeProfilePicture} color="primary">Change Profile Picture</Button>
        </Grid>
        <Grid item xs={12} hidden={passwordButtonHidden}>
        <Button fullWidth variant="outlined" onClick={handleChangePassword} color="primary">Change Password</Button>
        </Grid>
        <Grid item xs={12} hidden={passwordPanelHidden}>
        <TextField fullWidth label="Old Password" value={pass} onChange={(event)=>{setPass(event.target.value)}} type="password"  name="password" variant="outlined"/>
        <TextField fullWidth label="New Password" value={newPass} onChange={(event)=>{setNewPass(event.target.value)}} type="password"  name="new_password" variant="outlined"/>
        <Button fullWidth variant="contained" onClick={handleSaveChangePassword} color="primary">Save</Button>
        <Button fullWidth variant="contained" onClick={handleCloseChangePassword} color="default">Cancel</Button>
        </Grid>
        </Grid>
        <Grid item xs={6}>
        <TextField fullWidth label="First Name" onBlur={updateDetailsField} value={firstName} onChange={(event)=>{setFirstName(event.target.value)}} type="name"  name="first_name" variant="outlined"/>
        </Grid>
        <Grid item xs={6}>
        <TextField fullWidth label="Last Name" value={lastName} onBlur={updateDetailsField} onChange={(event)=>{setLastName(event.target.value)}} type="name"  name="last_name" variant="outlined"/>
        </Grid>
        <Grid item xs={12}>
        <TextField fullWidth label="Address" value={address} onBlur={updateDetailsField} onChange={(event)=>{setAddress(event.target.value)}} type="address"  name="address" variant="outlined"/>
        </Grid>
        <Grid item xs={12}>
        <CitySelect city={userDetails.city_id} />
        </Grid>
        <Grid item xs={6}>
        <TextField fullWidth label="PIN" value={pin} onBlur={updateDetailsField} onChange={(event)=>{setPin(event.target.value)}} type="number"  name="pin" variant="outlined"/>
        </Grid>
        <Grid item xs={6}>
        <TextField fullWidth label="Phone" value={phone} onBlur={updateDetailsField} onChange={(event)=>{setPhone(event.target.value)}} type="phone"  name="phone" variant="outlined"/>
        </Grid>
        </Grid>
        </Grid>
        <Grid item xs={12} style={{height:"25px"}}>
        </Grid>
        </form>
        </Paper>
        </Grid>
        <Grid item sm={4}>
        <Paper style={section} elevation={5}>
        <form >
        <Grid container justify="center" spacing={2}>
        <Grid container item xs={10} justify="center" spacing={2}>
            <Grid item xs={12} style={{height:'25px'}}></Grid>
        <Grid item xs={6}>
        <TextField fullWidth label="Alternate Phone" value={altPhone} onBlur={updateDetailsField} onChange={(event)=>{setAltPhone(event.target.value)}} type="phone"  name="alt_phone" variant="outlined"/>
        </Grid>
        <Grid item xs={6}>
        <TextField fullWidth label="Alternate Email" value={altEmail} onBlur={updateDetailsField} onChange={(event)=>{setAltEmail(event.target.value)}} type="email"  name="alt_email" variant="outlined"/>
        </Grid>
        <Grid item xs={6}>
        <TextField fullWidth label="Father's Name" onBlur={updateDetailsField} value={fathersName} onChange={(event)=>{setFathersName(event.target.value)}} type="name"  name="fathers_name" variant="outlined"/>
        </Grid>
        <Grid item xs={6}>
        <TextField fullWidth label="Father's Occupation" value={fathersOccupation} onBlur={updateDetailsField} onChange={(event)=>{setFathersOccupation(event.target.value)}} type="text"  name="fathers_occupation" variant="outlined"/>
        </Grid>
        <Grid item xs={6}>
        <TextField fullWidth label="Mother's Name" onBlur={updateDetailsField} value={mothersName} onChange={(event)=>{setMothersName(event.target.value)}} type="name"  name="mothers_name" variant="outlined"/>
        </Grid>
        <Grid item xs={6}>
        <TextField fullWidth label="Mother's Occupation" value={mothersOccupation} onBlur={updateDetailsField} onChange={(event)=>{setMothersOccupation(event.target.value)}} type="text"  name="mothers_occupation" variant="outlined"/>
        </Grid>
        <Grid item xs={6}>
        <TextField fullWidth label="Date of Birth" onBlur={updateDetailsField} value={dateOfBirth} onChange={(event)=>{setDateOfBirth(event.target.value)}} type="date"  name="dateofbirth" variant="outlined"/>
        </Grid>
            <Grid item sm={6}>
            <FormControl variant="outlined" fullWidth>
        <InputLabel>
          Gender
        </InputLabel>
        <Select
          value={gender}
          onBlur={updateDetailsField}
          name="gender"
          onChange={(event)=>{setGender(event.target.value);}}
        >
          <MenuItem value='M'>Male</MenuItem>
          <MenuItem value='F'>Female</MenuItem>
          <MenuItem value='C'>Trans Genders/Cross Dressers</MenuItem>
          <MenuItem value='O'>Other</MenuItem>
        </Select>
      </FormControl>
            </Grid>
            <Grid item sm={4}>
            <FormControl variant="outlined" fullWidth>
        <InputLabel>
          Marital Status
        </InputLabel>
        <Select
        onBlur={updateDetailsField}
        name="maritalstatus"
          value={maritalstatus}
          onChange={(event)=>{setMaritalstatus(event.target.value);}}
        >
          <MenuItem value='Married'>Married</MenuItem>
          <MenuItem value='Bachelor'>Bachelor</MenuItem>
          <MenuItem value='Divorcee'>Divorcee</MenuItem>
        </Select>
      </FormControl>
            </Grid>
        <Grid item xs={4}>
        <TextField fullWidth onBlur={updateDetailsField} label="Caste" value={caste} onChange={(event)=>{setCaste(event.target.value)}} type="text"  name="caste" variant="outlined"/>
        </Grid>
        <Grid item xs={4}>
        <MothertongueSelect onBlur={updateDetailsField} name="mothertongue_id" mothertongue={userDetails.mothertongue_id}/>
        </Grid>
        <Grid item xs={6}>
        <TextField fullWidth label="Aadhaar Number" onBlur={updateDetailsField} value={adhaarNo} onChange={(event)=>{setAdhaarNo(event.target.value)}} type="number"  name="aadhar_no" variant="outlined"/>
        </Grid>
        <Grid item xs={6}>
        <TextField fullWidth label="Passport Number" onBlur={updateDetailsField} value={passportNo} onChange={(event)=>{setPassportNo(event.target.value)}} type="text"  name="passport_no" variant="outlined"/>
        </Grid>
            <Grid item sm={4}>
            <FormControl variant="outlined" fullWidth>
        <InputLabel>
          Blood Group
        </InputLabel>
        <Select
        onBlur={updateDetailsField}
            name="bloodgroup"
          value={bloodgroup}
          onChange={(event)=>{setBloodgroup(event.target.value);}}
        >
          <MenuItem value='A+'>A+</MenuItem>
          <MenuItem value='A-'>A-</MenuItem>
          <MenuItem value='B+'>B+</MenuItem>
          <MenuItem value='B-'>B-</MenuItem>
          <MenuItem value='AB+'>AB+</MenuItem>
          <MenuItem value='AB-'>AB-</MenuItem>
          <MenuItem value='O+'>O+</MenuItem>
          <MenuItem value='O-'>O-</MenuItem>
          <MenuItem value='Hh'>Hh</MenuItem>
        </Select>
      </FormControl>
            </Grid>
            <Grid item sm={4}>
            <FormControl variant="outlined" fullWidth>
        <InputLabel>
          Physically Challenged
        </InputLabel>
        <Select
            name="physical_challenge"
            onBlur={updateDetailsField}
          value={physicalChallenge}
          onChange={(event)=>{setPhysicalChallenge(event.target.value);}}
        >
          <MenuItem value='true'>Yes</MenuItem>
          <MenuItem value='false'>No</MenuItem>
        </Select>
      </FormControl>
            </Grid>
            <Grid item xs={4}>
        <TextField fullWidth onBlur={updateDetailsField} label="Percentage (PH)" value={percentagePh} onChange={(event)=>{setPercentagePh(event.target.value)}} type="number"  name="percentage_ph" variant="outlined"/>
        </Grid>
        </Grid>
        </Grid>
        <Grid item xs={12} style={{height:"25px"}}>
        </Grid>
        </form>
        </Paper>
        </Grid>
        <Grid item sm={4}>
        <Paper style={section} elevation={5}>
        <form >
        <Grid container justify="center" spacing={2}>
        <Grid container item xs={10} justify="center" spacing={2}>
            <Grid item xs={12} style={{height:'25px'}}></Grid>
        <Grid item xs={12}>
        <TextField fullWidth label="Resume Heading" onBlur={updateDetailsField} value={resumeHeading} onChange={(event)=>{setResumeHeading(event.target.value)}} type="text"  name="resume_heading" variant="outlined"/>
        </Grid>
        <Grid item xs={8}>
        <TextField fullWidth label="About Me" onBlur={updateDetailsField} value={aboutMe} onChange={(event)=>{setAboutMe(event.target.value)}} type="text" multiline  name="about_me" variant="outlined"/>
        </Grid>
        <Grid item xs={4}>
        <TextField fullWidth label="Expected CTC" onBlur={updateDetailsField} value={expectedCTC} onChange={(event)=>{setExpectedCTC(event.target.value)}} type="number"  name="expected_ctc" variant="outlined"/>
        </Grid>
        <Grid item xs={6}>
        <TextField fullWidth label="Facebook Link" onBlur={updateDetailsField} value={facebookLink} onChange={(event)=>{setFacebookLink(event.target.value)}} type="url"  name="facebook_link" variant="outlined"/>
        </Grid>
        <Grid item xs={6}>
        <TextField fullWidth label="LinkedIn Link" onBlur={updateDetailsField} value={linkedinLink} onChange={(event)=>{setLinkedInLink(event.target.value)}} type="url"  name="linkedin_link" variant="outlined"/>
        </Grid>
        <Grid item xs={6}>
        <TextField fullWidth label="Google Link" onBlur={updateDetailsField} value={googleLink} onChange={(event)=>{setGoogleLink(event.target.value)}} type="url"  name="google_link" variant="outlined"/>
        </Grid>
        <Grid item xs={6}>
        <TextField fullWidth label="Website" onBlur={updateDetailsField} value={website} onChange={(event)=>{setWebsite(event.target.value)}} type="url"  name="website" variant="outlined"/>
        </Grid>
        </Grid>
        </Grid>
        <Grid item xs={12} style={{height:"25px"}}>
        </Grid>
        </form>
        </Paper>
        </Grid>
        <Grid item md={4}>
        <Snackbar open={showError} autoHideDuration={6000} onClose={()=>{showErrorSet(false);}}>
        
        <MuiAlert variant="filled" onClose={()=>{showErrorSet(false);}} severity="error">
        {error}
        </MuiAlert>
        </Snackbar>
        </Grid>
        <Grid item sm={4}>
        
        </Grid>
        <Grid item sm={4}>
        
        </Grid>
        </Grid>
        );
    })
    
    class Profile extends React.Component{
        userDetails={};
        loaded=false;
        constructor(props){
            super(props);
                apiInstance.requestWithAuth("private/select_user_details",{})
                .then((result)=>{
                    if(result.status===1){
                       result.dateofbirth=yyyymmdd(result.dateofbirth?
                       (new Date(result.dateofbirth)):
                       (new Date()));
                       console.dir(result);
                       this.userDetails=JSON.stringify(result);
                       this.loaded=true;
                       this.forceUpdate();
                        //TODO: add more
                    }
                });
        }
        render(){
            return(
                <Grid container spacing={1}>
                <Grid item sm={12}>
                    {this.loaded?<ProfileComponent data={this.userDetails} />:null}
                </Grid>
                <Grid item sm={6}>
                <Academic />
                </Grid>
                <Grid item sm={6}>
                <Professional />
                </Grid>
                </Grid>
            )
        }
    }
    export {Profile,CitySelect,MothertongueSelect,yyyymmdd};