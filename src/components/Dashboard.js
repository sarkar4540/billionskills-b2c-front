import { Paper } from "@material-ui/core";
import React from "react";
import "./styles.css";
import {Skill} from "./Skill";

function Dashboard(props){
    return (
        <Paper><Skill /> </Paper>
    );
}

export {Dashboard};