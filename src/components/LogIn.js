import { Snackbar,Paper, Grid, TextField, Button, ThemeProvider, Typography } from "@material-ui/core";
import MuiAlert from '@material-ui/lab/Alert';
import React,{useState} from "react";
import "./styles.css";
import {theme} from "./";
import { withRouter, Route, Switch } from "react-router-dom";

import apiInstance from "../api";


function SignUp(props){
    const [error,setError] = useState(''),[showError,showErrorSet] = useState(false);
    const [email,setEmail]=useState(''),
    [pass,setPass]=useState(''),
    [firstName,setFirstName]=useState(''),
    [lastName,setLastName]=useState(''),
    [phone,setPhone]=useState('');
    function handleLogIn(){
        if(email !== '' && pass !== ''){
            apiInstance.request('private/signup',{
                email:email,
                password:pass,
                first_name:firstName,
                last_name:lastName,
                phone:phone
            })
            .then((result) =>{
                if(result.status===4){
                    setError("Error: "+result.error);
                    showErrorSet(true);
                }
                else if(result.status===2){
                    setError("The provided email is already registered.");
                    showErrorSet(true);
                }
                else if(result.status===0){
                    setError("Some error occurred. Contact Support");
                    showErrorSet(true);
                }
                else if(result.status===1){
                    apiInstance.logIn(email,pass)
                    .then((result) =>{
                        if(result.status===1){
                            props.history.push('/');
                            document.location.reload();
                        }
                        else{
                            setError("Unknown error: "+result);
                            showErrorSet(true);
                        }
                    },
                    (error)=>{
                        setError("Seems like you are disconnected! "+error);
                        showErrorSet(true);
                    });
                }
                else{
                    setError("Unknown error: "+result);
                    showErrorSet(true);
                }
            },
            (error)=>{
                setError("Seems like you are disconnected! "+error);
                showErrorSet(true);
            });
        }
    }
    return (
        <ThemeProvider theme={theme}>
        <Grid container alignItems="center" justify="center" spacing={3}>
        <Grid item xs={12} style={{height:"125px"}}>
        </Grid>
        <Grid item md={4}>
        <Paper>
        <form >
        <Grid container justify="center" spacing={2}>
        <Grid item xs={12} style={{textAlign:"center"}}>
        <img style={{width:"75px",position:"relative",padding:"10px"}} src='logo.png' alt="BillionSkills"/>
        </Grid>
        <Grid item container spacing={2} xs={10}>
        <Grid item xs={12}>
        <TextField fullWidth label="Email" value={email} onChange={(event)=>{setEmail(event.target.value)}} type="email" id="email" variant="outlined"/>
        </Grid>
        <Grid item xs={12}>
        <TextField fullWidth label="Password" value={pass} onChange={(event)=>{setPass(event.target.value)}} type="password"  id="password" variant="outlined"/>
        </Grid>
        <Grid item xs={12}>
        <TextField fullWidth label="First Name" value={firstName} onChange={(event)=>{setFirstName(event.target.value)}} type="name"  id="f_name" variant="outlined"/>
        </Grid>
        <Grid item xs={12}>
        <TextField fullWidth label="Last Name" value={lastName} onChange={(event)=>{setLastName(event.target.value)}} type="name"  id="l_name" variant="outlined"/>
        </Grid>
        <Grid item xs={12}>
        <TextField fullWidth label="Phone" value={phone} onChange={(event)=>{setPhone(event.target.value)}} type="phone"  id="phone" variant="outlined"/>
        </Grid>
        <Grid item xs={12}>
        <Button fullWidth variant="contained" onClick={handleLogIn} color="primary">Continue</Button>
        </Grid>
        </Grid>
        <Grid item xs={12} style={{height:"25px"}}>
        </Grid>
        </Grid>
        </form>
        </Paper>
        </Grid>
        <Grid item xs={12} style={{height:"25px"}}>
        </Grid>
        <Grid item md={4}>
        <Snackbar open={showError} autoHideDuration={6000} onClose={()=>{showErrorSet(false);}}>
        
        <MuiAlert variant="filled" onClose={()=>{showErrorSet(false);}} severity="error">
        {error}
        </MuiAlert>
        </Snackbar>
        </Grid>
        </Grid>
        </ThemeProvider>
        );
    }
    
    function LogIn(props){
        
        const [error,setError] = useState(''),[showError,showErrorSet] = useState(false);
        const [email,setEmail]=useState(''),
        [pass,setPass]=useState('');
        
        function handleLogIn(){
            if(email !== '' && pass !== ''){
                apiInstance.logIn(email,pass)
                .then((result) =>{
                    if(result.status===4){
                        setError("Error: "+result.error);
                        showErrorSet(true);
                    }
                    else if(result.status===3){
                        setError("Profile deactivated. Contact support.");
                        showErrorSet(true);
                    }
                    else if(result.status===2){
                        setError("The provided email is not registered.");
                        showErrorSet(true);
                    }
                    else if(result.status===0){
                        setError("Invalid password provided.");
                        showErrorSet(true);
                    }
                    else if(result.status===1){
                        document.location.reload();
                    }
                    else{
                        setError("Unknown error: "+result);
                        showErrorSet(true);
                    }
                },
                (error)=>{
                    setError("Seems like you are disconnected! "+error);
                    showErrorSet(true);
                });
            }
        }
        return (
            <Grid container alignItems="center" justify="center" spacing={3}>
            <Grid item xs={12} style={{height:"125px"}}>
            </Grid>
            <Grid item md={4}>
            <Paper>
            <form >
            <Grid container justify="center" spacing={2}>
            <Grid item xs={12} style={{textAlign:"center"}}>
            <img style={{width:"75px",position:"relative",padding:"10px"}} src='logo.png' alt="BillionSkills"/>
            </Grid>
            <Grid item container spacing={2} xs={10}>
            <Grid item xs={12}>
            <TextField fullWidth label="Email" value={email} onChange={(event)=>{setEmail(event.target.value)}} type="email" id="email" variant="outlined"/>
            </Grid>
            <Grid item xs={12}>
            <TextField fullWidth label="Password" value={pass} onChange={(event)=>{setPass(event.target.value)}} type="password"  id="password" variant="outlined"/>
            </Grid>
            <Grid item xs={12}>
            <Button fullWidth variant="contained" onClick={handleLogIn} color="primary">Sign In</Button>
            </Grid>
            </Grid>
            <Grid item xs={12}>
            <Typography variant="button" display="block" align="center">OR</Typography>
            </Grid>
            <Grid item container spacing={2} xs={10}>
            <Grid item xs={6}>
            <Button fullWidth variant="contained" onClick={()=>{props.history.push('/signup')}} color="secondary">Sign Up</Button>
            </Grid>
            <Grid item xs={6}>
            <Button fullWidth variant="contained" color="default">Forgot Password ?</Button>
            </Grid>
            </Grid>
            <Grid item xs={12} style={{height:"25px"}}>
            </Grid>
            </Grid>
            </form>
            </Paper>
            </Grid>
            <Grid item xs={12} style={{height:"25px"}}>
            </Grid>
            <Grid item md={4}>
            <Snackbar open={showError} autoHideDuration={6000} onClose={()=>{showErrorSet(false);}}>
            
            <MuiAlert variant="filled" onClose={()=>{showErrorSet(false);}} severity="error">
            {error}
            </MuiAlert>
            </Snackbar>
            </Grid>
            </Grid>
            );
        }
        var UserApp = withRouter((props)=>{
            if(!['/','/signup'].includes(props.location.pathname)){
                props.history.push('/');

            }
            return (
                <ThemeProvider theme={theme}>
                <Switch>
                <Route path='/' exact component={LogIn} />
                <Route path='/signup' component={SignUp} />
                </Switch>
                </ThemeProvider>
                );
            })
            export {LogIn,SignUp,UserApp};