import { Grid } from "@material-ui/core";
import React from "react";
import "./styles.css";
import {Project} from "./Projects";
import {Publication} from "./Publications";
import { Research } from "./Research";
import { Award } from "./Awards";
import { Hobby } from "./Hobby";

function Portfolio(props){
    return (
        <Grid container spacing={3}>
            <Grid item xs={6} >
                <Research />
            </Grid>
            <Grid item xs={6} >
                <Hobby />
            </Grid>
            <Grid item xs={6}>
                <Award />
            </Grid>
            <Grid item xs={6}>
                <Project />
            </Grid>
            <Grid item xs={6}>
                <Publication />
            </Grid>
        </Grid>
    );
}

export {Portfolio};