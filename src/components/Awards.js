import { Paper,Grid, Typography, List, ListItem, Divider, TextField, Button, Dialog } from "@material-ui/core";
import React from "react";
import "./styles.css";
import apiInstance from "../api";



function AwardView(props){
    let data=props.data;
    return (
        <Grid component={Paper} style={{padding:"10px"}} container>
        <Grid item xs={10}>
        <Typography variant="h5">
        {data.title}
        </Typography>
        <Typography variant="subtitle1">
        {data.detail}
        </Typography>
        </Grid>
        <Grid item xs={2}>
        <Button onClick={(e)=> {e.stopPropagation();window.open(data.file_url, "_blank")}} fullWidth variant="outlined" color="default">OPEN LINK</Button>
        </Grid>
        <Grid item xs={12}>
        </Grid>
        </Grid>
        );
    }
    
    
    class Award extends React.Component{
        constructor(props){
            super(props);
            this.state=({
                projects:[],
                editOpen:false,
                    title:'',
                    detail:'',
                    file_url:'',
                    id:0,
                operation:'',
                error_msg:null
            });
            this.setState=this.setState.bind(this);
            this.handleEditClose=this.handleEditClose.bind(this);
            this.handleListClick=this.handleListClick.bind(this);
            this.handleAddClick=this.handleAddClick.bind(this);
            this.handleSubmit=this.handleSubmit.bind(this);
            this.handleDelete=this.handleDelete.bind(this);
        }
        componentDidMount(){
            (async ()=>{
                let response=await apiInstance.requestWithAuth("private/award",{id:0});
                if(response && !response.status && !response.error_msg){
                    this.setState({projects:response});
                }
                else this.setState({projects:[]});
            })();
        }
        async handleEditClose(){
            this.setState({editOpen:false});
            let response=await apiInstance.requestWithAuth("private/award",{id:0});
            if(response && !response.status && !response.error_msg){
                this.setState({projects:response});
            }
            else this.setState({projects:[]});
        }
        handleListClick(data){
            data.editOpen=true;
            data.operation="update";
            data.error_msg=null;
            this.setState(data);
        }
        handleAddClick(){
            this.setState({
                editOpen:true,
                title:'',
                detail:'',
                file_url:'',
                id:0,
                operation:"insert",
                error:''
            });
        }
        async handleSubmit(){
            let response=await apiInstance.requestWithAuth("private/dynamic_award",this.state);
            if(response && response.status===1){
                this.handleEditClose();
            }
            else this.setState({error:"All fields are mandatory."});
        }
        async handleDelete(){
            await this.setState({operation:"delete"});
            let response=await apiInstance.requestWithAuth("private/dynamic_award",this.state);
            if(response && response.status===1){
                this.handleEditClose();
            }
            else this.setState({error:response.error});
        }
        render(){
            return (
                <Paper elevation={5}>
                <Grid container>
                <Grid item xs={10}>
                <Typography align="left" variant="h4" style={{padding:"10px"}}>
                Awards &amp; Certifications
                </Typography>
                
                </Grid>
                <Grid item xs={2}>
                <Button fullWidth variant='outlined' onClick={this.handleAddClick} color='primary'>Add</Button>
                </Grid>
                </Grid>
                <Divider />
                <List alignItems="center" >
                {
                    this.state.projects.length>0?this.state.projects.map((project)=>{
                        return <ListItem button onClick={()=>{this.handleListClick(project)}}><AwardView data={project} /></ListItem>
                    }):(<ListItem><Typography variant="subtitle1">No Award Records.</Typography></ListItem>)
                }
                </List>
                {/*this.state.editOpen?(<AcademicEdit open={this.state.editOpen} streams={this.state.streams} degrees={this.state.degrees} data={this.state} operation={this.state.operation} onClose={this.handleEditClose} />):(null)*/}
                
                <Dialog open={this.state.editOpen} onClose={this.handleEditClose}>
                <div  style={{height:"100%",padding:"15px"}}>
                <Grid container spacing={1}>
                <Grid item xs={12}>
                <TextField fullWidth label="Title" value={this.state.title} onChange={async (event)=>{await this.setState({title:event.target.value});}} type="text"  name="title" variant="outlined"/>
                </Grid>
                <Grid item xs={8}>
                <TextField fullWidth label="Details" multiline value={this.state.detail} onChange={async (event)=>{await this.setState({detail:event.target.value});}} type="text"  name="detail" variant="outlined"/>
                </Grid>
                <Grid item xs={4}>
                <TextField fullWidth label="Associated Link" value={this.state.file_url} onChange={async (event)=>{await this.setState({file_url:event.target.value});}} type="url"  name="file_url" variant="outlined"/>
                </Grid>
                {this.state.operation==='insert'?(
                    <Grid item container spacing={1} xs={12}>
                    <Grid item xs={8}>
                    <Button fullWidth variant="contained" onClick={this.handleSubmit} color="primary">Add</Button>
                    </Grid>
                    <Grid item xs={4}>
                    <Button fullWidth variant="outlined" onClick={this.handleEditClose} color="default">Cancel</Button>
                    </Grid>
                    </Grid>
                    ):(
                        <Grid item container spacing={1} xs={12}>
                        <Grid item xs={4}>
                        <Button fullWidth variant="contained" onClick={this.handleSubmit} color="primary">Update</Button>
                        </Grid>
                        <Grid item xs={4}>
                        <Button fullWidth variant="outlined" onClick={this.handleDelete} color="primary">Delete</Button>
                        </Grid>
                        <Grid item xs={4}>
                        <Button fullWidth variant="outlined" onClick={this.handleEditClose} color="default">Cancel</Button>
                        </Grid>
                        </Grid>
                        )}
                        </Grid>
                        <Typography variant="body2" align="center" color="error">
                            {this.state.error}
                        </Typography>

                        </div>
                        </Dialog>
                        </Paper>
                        );
                    }
                }
                
                export {Award};