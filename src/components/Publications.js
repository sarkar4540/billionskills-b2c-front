import { Paper,Grid, Typography, List, ListItem, Divider, FormControl, InputLabel, Select, MenuItem, TextField, Button, Dialog } from "@material-ui/core";
import React from "react";
import "./styles.css";
import apiInstance from "../api";



function PublicationView(props){
    let data=props.data;
    return (
        <Grid component={Paper} style={{padding:"10px"}} container>
        <Grid item xs={10}>
        <Typography variant="h5">
        {data.title}
        </Typography>
        <Typography variant="subtitle1">
        {data.detail}
        </Typography>
        </Grid>
        <Grid item xs={2}>
        <Button onClick={(e)=> {e.stopPropagation();window.open(data.url, "_blank")}} fullWidth variant="outlined" color="default">OPEN LINK</Button>
        </Grid>
        <Grid item xs={6}>
        <Typography variant="body1">
        Organization(s): {data.client}
        </Typography>
        <Typography variant="body1">
        Author(s): {data.author}
        </Typography>
        </Grid>
        <Grid item xs={6}>
        <Typography variant="body1" style={{fontWeight:"bold"}} >
        {data.type}
        </Typography>
        <Typography variant="body1">
        Published in {data.publish_year}
        </Typography>
        <Typography variant="body1">
        Version: {data.version}
        </Typography>
        </Grid>
        <Grid item xs={12}>
        </Grid>
        </Grid>
        );
    }
    
    
    class Publication extends React.Component{
        constructor(props){
            super(props);
            this.state=({
                publications:[],
                editOpen:false,
                    title:'',
                    type:'',
                    client:'',
                    author:'',
                    publish_year:'',
                    catation:'',
                    version:'',
                    url:'',
                    detail:'',
                    file_url:'',
                    id:0,
                operation:'',
                error_msg:null
            });
            this.setState=this.setState.bind(this);
            this.handleEditClose=this.handleEditClose.bind(this);
            this.handleListClick=this.handleListClick.bind(this);
            this.handleAddClick=this.handleAddClick.bind(this);
            this.handleSubmit=this.handleSubmit.bind(this);
            this.handleDelete=this.handleDelete.bind(this);
        }
        componentDidMount(){
            (async ()=>{
                let response=await apiInstance.requestWithAuth("private/publication",{id:0});
                if(!response.status && !response.error_msg){
                    this.setState({publications:response});
                }
                else this.setState({publications:[]});
            })();
        }
        async handleEditClose(){
            this.setState({editOpen:false});
            let response=await apiInstance.requestWithAuth("private/publication",{id:0});
            if(!response.status && !response.error_msg){
                this.setState({publications:response});
            }
            else this.setState({publications:[]});
        }
        handleListClick(data){
            data.editOpen=true;
            data.operation="update";
            data.error_msg=null;
            this.setState(data);
        }
        handleAddClick(){
            this.setState({
                editOpen:true,
                title:'',
                type:'',
                client:'',
                author:'',
                publish_year:'',
                catation:'',
                version:'',
                url:'',
                detail:'',
                file_url:'',
                id:0,
                operation:"insert",
                error:''
            });
        }
        async handleSubmit(){
            let response=await apiInstance.requestWithAuth("private/dynamic_publication",this.state);
            if(response.status===1){
                this.handleEditClose();
            }
            else this.setState({error:"All fields are mandatory."});
        }
        async handleDelete(){
            await this.setState({operation:"delete"});
            let response=await apiInstance.requestWithAuth("private/dynamic_publication",this.state);
            if(response.status===1){
                this.handleEditClose();
            }
            else this.setState({error:response.error});
        }
        render(){
            return (
                <Paper elevation={5}>
                <Grid container>
                <Grid item xs={10}>
                <Typography align="left" variant="h4" style={{padding:"10px"}}>
                Publications
                </Typography>
                
                </Grid>
                <Grid item xs={2}>
                <Button fullWidth variant='outlined' onClick={this.handleAddClick} color='primary'>Add</Button>
                </Grid>
                </Grid>
                <Divider />
                <List alignItems="center" >
                {
                    this.state.publications.length>0?this.state.publications.map((publication)=>{
                        return <ListItem button onClick={()=>{this.handleListClick(publication)}}><PublicationView data={publication} /></ListItem>
                    }):(<ListItem><Typography variant="subtitle1">No Publication Records.</Typography></ListItem>)
                }
                </List>
                {/*this.state.editOpen?(<AcademicEdit open={this.state.editOpen} streams={this.state.streams} degrees={this.state.degrees} data={this.state} operation={this.state.operation} onClose={this.handleEditClose} />):(null)*/}
                
                <Dialog open={this.state.editOpen} onClose={this.handleEditClose}>
                <div  style={{height:"100%",padding:"15px"}}>
                <Grid container spacing={1}>
                <Grid item xs={12}>
                <TextField fullWidth label="Title" value={this.state.title} onChange={async (event)=>{await this.setState({title:event.target.value});}} type="text"  name="title" variant="outlined"/>
                </Grid>
                <Grid item sm={6}>
                <FormControl variant="outlined" fullWidth>
                <InputLabel>
                Type
                </InputLabel>
                <Select
                value={this.state.type}
                onChange={async (event)=>{await this.setState({type:event.target.value});}}
                >
                <MenuItem value="Patent" key="type">Patent</MenuItem>
                <MenuItem value="Journal" key="type">Journal</MenuItem>
                <MenuItem value="Paper" key="type">Paper</MenuItem>
                <MenuItem value="Book" key="type">Book</MenuItem>
                <MenuItem value="Other" key="type">Other</MenuItem>
                </Select>
                </FormControl>
                </Grid>
                <Grid item xs={6}>
                <TextField fullWidth label="Author(s)" value={this.state.author} onChange={async (event)=>{await this.setState({author:event.target.value});}} type="name"  name="author" variant="outlined"/>
                </Grid>
                <Grid item xs={6}>
                <TextField fullWidth label="Abstract" multiline value={this.state.detail} onChange={async (event)=>{await this.setState({detail:event.target.value});}} type="text"  name="detail" variant="outlined"/>
                </Grid>
                <Grid item xs={6}>
                <TextField fullWidth label="Citations" multiline value={this.state.catation} onChange={async (event)=>{await this.setState({catation:event.target.value});}} type="text"  name="catation" variant="outlined"/>
                </Grid>
                <Grid item xs={6}>
                <TextField fullWidth label="Organization(s)" value={this.state.client} onChange={async (event)=>{await this.setState({client:event.target.value});}} type="name"  name="client" variant="outlined"/>
                </Grid>
                <Grid item xs={3}>
                <TextField fullWidth label="Year Published"  value={this.state.publish_year} onChange={async (event)=>{await this.setState({publish_year:event.target.value});}} type="number" size="4"  name="publish_year" variant="outlined"/>
                </Grid>
                <Grid item xs={3}>
                <TextField fullWidth label="Version"  value={this.state.version} onChange={async (event)=>{await this.setState({version:event.target.value});}} type="number" size="4"  name="version" variant="outlined"/>
                </Grid>
                <Grid item xs={6}>
                <TextField fullWidth label="Associated Link" value={this.state.url} onChange={async (event)=>{await this.setState({url:event.target.value});}} type="url"  name="url" variant="outlined"/>
                </Grid>
                <Grid item xs={6}>
                <TextField fullWidth label="File Link" value={this.state.file_url} onChange={async (event)=>{await this.setState({file_url:event.target.value});}} type="url"  name="file_url" variant="outlined"/>
                </Grid>
                {this.state.operation==='insert'?(
                    <Grid item container spacing={1} xs={12}>
                    <Grid item xs={8}>
                    <Button fullWidth variant="contained" onClick={this.handleSubmit} color="primary">Add</Button>
                    </Grid>
                    <Grid item xs={4}>
                    <Button fullWidth variant="outlined" onClick={this.handleEditClose} color="default">Cancel</Button>
                    </Grid>
                    </Grid>
                    ):(
                        <Grid item container spacing={1} xs={12}>
                        <Grid item xs={4}>
                        <Button fullWidth variant="contained" onClick={this.handleSubmit} color="primary">Update</Button>
                        </Grid>
                        <Grid item xs={4}>
                        <Button fullWidth variant="outlined" onClick={this.handleDelete} color="primary">Delete</Button>
                        </Grid>
                        <Grid item xs={4}>
                        <Button fullWidth variant="outlined" onClick={this.handleEditClose} color="default">Cancel</Button>
                        </Grid>
                        </Grid>
                        )}
                        </Grid>
                        <Typography variant="body2" align="center" color="error">
                            {this.state.error}
                        </Typography>

                        </div>
                        </Dialog>
                        </Paper>
                        );
                    }
                }
                
                export {Publication};