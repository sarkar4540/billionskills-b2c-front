import React from "react";
import {Tabs,Tab,AppBar,Typography,Grid,Toolbar,ThemeProvider,Paper, Button} from "@material-ui/core";
import { Switch, Route, Link, withRouter} from "react-router-dom";
import {theme, Dashboard, Profile, Portfolio, Jobs, Connects, Posts, Challenges, Programmes, Events} from "./components";
import './App.css';
import apiInstance from "./api";
const allTabs = ['/', '/profile', '/portfolio','/jobs','/connects','/posts','/challenges','/programmes','/events'];

function a11yProps(index){
    return {
        'value':allTabs[index],
        'component':Link,
        'to':allTabs[index]
    }
}

function NavBar(props) {
    
    return (
        <div>
        <AppBar position="sticky">
        <Toolbar>
        <Grid container spacing={1} alignItems="flex-end">
        <Grid item sm={3} {...a11yProps(0)}>
        <Paper elevation={0} className="logo-nav" square>
        <img src='logo.png' alt="BillionSkills"/>
        <Typography variant="h2" className="title" color="primary">BillionSkills</Typography>
        </Paper>
        </Grid>
        <Grid item sm={9}>
            <div className='logout-button-panel'>
                <Typography variant='button' className='logout-button-text'>
                    {apiInstance.getUserData().first_name} {apiInstance.getUserData().last_name} 
                </Typography>
                <Button variant='contained' color='secondary' onClick={()=>{
                    apiInstance.logOut();
                    document.location.reload();
                    }}>Log Out</Button>
            </div>
        <Tabs value={props.location} variant="scrollable">
        <Tab label="Profile" {...a11yProps(1)} />
        <Tab label="Portfolio" {...a11yProps(2)} />
        <Tab label="Jobs" {...a11yProps(3)} />
        <Tab label="Connects" {...a11yProps(4)} />
        <Tab label="Posts" {...a11yProps(5)} />
        <Tab label="Challenges" {...a11yProps(6)} />
        <Tab label="Programmes" {...a11yProps(7)} />
        <Tab label="Events" {...a11yProps(8)} />
        </Tabs>
        </Grid>
        </Grid>
        </Toolbar>
        </AppBar>
        </div>
        );
        
    }
    
    function App(props){
        if(!allTabs.includes(props.location.pathname)){
            props.history.push('/');
        }
        if(!apiInstance.isLoggedIn()){
            document.location.reload();
        }
        return (
            <div className="App">
            <ThemeProvider theme={theme}>
                <NavBar location={props.location.pathname} />
                <div className='main-panel'>
                <Switch>
                <Route path={allTabs[0]} exact component={Dashboard} />
                <Route path={allTabs[1]} component={Profile} />
                <Route path={allTabs[2]} component={Portfolio} />
                <Route path={allTabs[3]} component={Jobs} />
                <Route path={allTabs[4]} component={Connects} />
                <Route path={allTabs[5]} component={Posts} />
                <Route path={allTabs[6]} component={Challenges} />
                <Route path={allTabs[7]} component={Programmes} />
                <Route path={allTabs[8]} component={Events} />
                </Switch>
                </div>
                </ThemeProvider>
                </div>
                );
            }
        

    export default withRouter(App);
            
            